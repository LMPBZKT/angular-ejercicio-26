import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-boton2',
  templateUrl: './boton2.component.html',
  styleUrls: ['./boton2.component.css']
})
export class Boton2Component implements OnInit {

  textoBoton2: string = 'Hola soy el boton 2!';
  constructor(private Servicio: DataService) { }

  ngOnInit(): void {
  }

  cambiarBoton2(): void {
    this.Servicio.texto = this.textoBoton2
  }
}
